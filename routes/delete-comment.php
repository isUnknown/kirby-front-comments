<?php

function deleteComment()
{
    $jsonRequest = file_get_contents("php://input");
    $request = json_decode($jsonRequest, true);

    $dataFile = __DIR__ . '/../data/data.json';
    $jsonData = file_get_contents($dataFile);
    $data = json_decode($jsonData, true);

    foreach ($data as $index => $item) {
        if ($item['id'] === $request['id']) {
            array_splice($data, $index, 1);
            break;
        }
    }

    file_put_contents($dataFile, json_encode($data));

    return json_encode($data);
}
