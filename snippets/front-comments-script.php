<?php
$comments = $page->comments()->exists() && $page->comments()->isNotEmpty()
  ? $page->comments()->toData('yaml')
  : [];

$suggestions = $page->suggestions()->exists() && $page->suggestions()->isNotEmpty()
  ? $page->suggestions()->toData('yaml')
  : [];

foreach ($comments as &$comment) {
  $comment['message'] = nl2br($comment['message']);
}

foreach ($suggestions as &$suggestion) {
  $suggestion['message'] = nl2br($suggestion['message']);
}


$commentsJson = json_encode($comments, JSON_HEX_APOS);
$suggestionsJson = json_encode($suggestions, JSON_HEX_APOS);
?>
<script>
  const FCAuthor = '<?= e($kirby->user()->name()->isNotEmpty(), $kirby->user()->name(), $kirby->user()->email()) ?>';
  const FCTeam = '<?= $kirby->user()->team() ?>';
  const FCPageId = '<?= $page->panel()->id() ?>';
  const FCPageUri = '<?= $page->panel()->id() ?>';
  const FCCsrf = "<?= csrf() ?>";
  const FCComments = <?= $commentsJson ?>; 
  const FCSuggestions = <?= $suggestionsJson ?>; 
  const FCFilesPath = '<?= $filesPath ?>';
  const FCPosition = '<?= $position ?>';
</script>
<?php

echo js($filesPath . '/index.js');
