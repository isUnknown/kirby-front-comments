<?php

Kirby::plugin('adrienpayet/front-comments', [
  'options' => [
    'cache' => true,
  ],
  'hooks' => [
    'page.update:after' => function ($newPage) {
        kirby()
            ->cache('adrienpayet.front-comments')
            ->set('foo', 'bar');
    }
  ]
]);
