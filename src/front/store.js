const Store = {
  comments: [],
  suggestions: [],
  author: null,
  page: {
    id: null,
    uri: null,
  },
  csrf: null,
  save(key) {
    const data = {};
    data[key] = this[key];
    const init = {
      method: "PATCH",
      headers: {
        "X-CSRF": this.csrf,
      },
      body: JSON.stringify(data).replaceAll("_", ""),
    };

    return fetch("/api/pages/" + this.page.id, init)
      .then((response) => {
        if (!response.ok) {
          return Promise.reject(
            "Front comments plugin - can't add comment: " + response.statusText
          );
        }
        return response.json();
      })
      .then((response) => {
        console.log(response);
        console.log("Front comments plugin - comment successfully added.");
      })
      .catch((error) => {
        return Promise.reject(error);
      });
  },
};

export default Store;
