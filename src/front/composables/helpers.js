class Helpers {
  static fixOffscreen(node) {
    const margin = 16;

    if (!node) {
      console.error("fixOffscreen: node does not exist");
      return;
    }

    const XDiff =
      window.innerWidth +
      window.pageXOffset -
      (node.offsetLeft + node.offsetWidth + window.pageXOffset);

    const YDiff =
      window.innerHeight +
      window.pageYOffset -
      (node.offsetTop + node.offsetHeight);

    if (XDiff <= 0) {
      node.style.transform = `translateX(${XDiff - margin}px)`;
    }

    if (YDiff <= 0) {
      node.style.transform += `translateY(${YDiff - margin}px)`;
    }
  }
}

export default Helpers;
