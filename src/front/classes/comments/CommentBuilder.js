import Store from "../../store.js";
import dayjs from "dayjs";
import Helpers from "../../composables/helpers.js";
import Comment from "./Comment.js";

class CommentBuilder {
  constructor() {
    this.position = {};
    this.author = "";
    this.team = "";
    this.message = "";
    this.id = Date.now();
    this.date = dayjs().format("DD/MM/YY");
    this.time = dayjs().format("HH:mm");
    this.windowWidth = window.innerWidth;
    this.userAgent = navigator.userAgent;
  }

  /**
   *
   * @param {Object} position - An object with two properties: top and left.
   * @param {string} position.top - The vertical position of the comment in CSS format (e.g. "10px", "50%").
   * @param {string} position.left - The horizontal position of the comment in CSS format (e.g. "10px", "50%").
   * @returns {CommentBuilder} The CommentBuilder instance for method chaining.
   */
  setPosition(position) {
    this.position = position;
    return this;
  }

  setAuthor(author) {
    this.author = author;
    return this;
  }

  setTeam(team) {
    this.team = team;
    return this;
  }

  setMessage(message) {
    this.message = message;
    return this;
  }

  setId(id) {
    this.id = id;
    return this;
  }

  setDate(date) {
    this.date = date;
    return this;
  }

  setTime(time) {
    this.time = time;
    return this;
  }

  setWindowWidth(windowWidth) {
    this.windowWidth = windowWidth;
    return this;
  }

  setUserAgent(userAgent) {
    this.userAgent = userAgent;
    return this;
  }

  build() {
    if (!this.position || !this.author || !this.message) {
      console.error(
        "Missing required parameters for Comment: position, author, message",
        this
      );
      return null;
    }
    const comment = new Comment(
      this.position,
      this.author,
      this.message,
      this.id,
      this.date,
      this.time,
      this.windowWidth,
      this.userAgent,
      this.team
    );

    return comment;
  }
}

export default CommentBuilder;
