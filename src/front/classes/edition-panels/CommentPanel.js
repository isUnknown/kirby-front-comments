import Panel from "./Panel.js";
import Store from "../../store.js";
import Comment from "../comments/Comment.js";
import CommentBuilder from "../comments/CommentBuilder.js";

class CommentPanel extends Panel {
  create() {
    super.create();
    document
      .querySelector(".fc__edition-panel__save-btn")
      .addEventListener("click", () => {
        this._createComment();
      });
    this._textArea.addEventListener("keydown", (event) => {
      if (event.key === "Enter" && !event.shiftKey) {
        this._createComment();
      }
      if (event.key === "Escape") {
        document.body.removeChild(this._panel);
      }
    });
  }

  async _createComment() {
    const comment = new CommentBuilder()
      .setAuthor(Store.author)
      .setTeam(Store.team)
      .setMessage(this._textArea.value)
      .setPosition({
        top: this._getPos().top,
        left: this._getPos().left,
      })
      .build();

    try {
      Store.comments.push(comment);
      await Store.save("comments");
      comment.toBubble();
      document.body.removeChild(this._panel);
    } catch (error) {
      Store.comments = Store.comments.filter(
        (storedComment) => storedComment.id !== comment.id
      );
      document.body.removeChild(comment.node);
      console.error("Create comment failed:", error);
    }

    return comment;
  }
}

export default CommentPanel;
