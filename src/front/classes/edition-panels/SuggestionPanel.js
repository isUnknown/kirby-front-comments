import Panel from "./Panel.js";
import Store from "../../store.js";
import Suggestion from "../Suggestion.js";

class SuggestionPanel extends Panel {
  constructor(field) {
    super();
    this._field = field;
    this._selection = window.getSelection();
    this._target = this._selection.toString();
  }
  create() {
    this._highlight();
    this._context = this._getContext();
    super.create();
    document
      .querySelector(".fc__edition-panel__save-btn")
      .addEventListener("click", () => {
        this._createSuggestion();
      });
    this._textArea.addEventListener("keydown", (event) => {
      if (event.key === "Enter") {
        this._createSuggestion();
      }
      if (event.key === "Escape") {
        document.body.removeChild(this._panel);
      }
    });
  }

  _createSuggestion() {
    const suggestion = new Suggestion(
      Store.author,
      this._context,
      this._target,
      this._textArea.value,
      this._field.dataset.fieldName
    );
    Store.suggestions.push(suggestion);
    Store.save("suggestions");
    document.body.removeChild(this._panel);
    return suggestion;
  }

  _highlight() {
    if (this._selection.toString() !== "") {
      const range = this._selection.getRangeAt(0);
      const span = document.createElement("span");
      span.className = "fc__suggestion--edit";
      range.surroundContents(span);
    }
  }

  _getContext() {
    if (this._selection.toString() !== "") {
      const textNode = this._selection.anchorNode;
      const textContent = textNode.textContent;
      const selectedText = this._selection.toString();
      const index = textContent.indexOf(selectedText);
      const before = textContent.slice(Math.max(0, index - 30), index);
      const after = textContent.slice(
        index + selectedText.length,
        index + selectedText.length + 30
      );
      const context = {
        before: before,
        after: after,
      };
      return context;
    }
  }
}

export default SuggestionPanel;
