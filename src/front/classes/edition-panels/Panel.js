import Comment from "../comments/Comment.js";
import Store from "../../store.js";
import Helpers from "../../composables/helpers.js";

class Panel {
  constructor() {
    this._panel;
    this._textArea;
  }

  create() {
    this._panel = this._createPanel();
    this._textArea = this._createTextArea();
    const btns = this._createBtns();
    this._panel.appendChild(this._textArea);
    this._panel.appendChild(btns);
    document.body.appendChild(this._panel);
    this._textArea.focus();

    Helpers.fixOffscreen(this._panel);

    document
      .querySelector(".fc__edition-panel__remove-btn")
      .addEventListener("click", () => {
        document.body.removeChild(this._panel);
      });
  }

  _createPanel() {
    const panel = document.createElement("div");
    panel.classList.add("fc__edition-panel");

    const panelWidth = 240;
    const panelHeight = 300;

    panel.style.left = this._getMousePos().x + "px";
    panel.style.top = this._getMousePos().y + window.scrollY + "px";

    return panel;
  }

  _createBtns() {
    const btns = document.createElement("div");
    btns.classList.add("fc__edition-panel__btns");

    const removeBtn = document.createElement("button");
    removeBtn.classList.add("fc__edition-panel__remove-btn");
    removeBtn.textContent = "supprimer";

    const saveBtn = document.createElement("button");
    saveBtn.classList.add("fc__edition-panel__save-btn");
    saveBtn.textContent = "enregistrer";

    btns.appendChild(removeBtn);
    btns.appendChild(saveBtn);

    return btns;
  }

  _getMousePos() {
    return {
      x: event.clientX,
      y: event.clientY,
    };
  }

  _createTextArea() {
    const textArea = document.createElement("textarea");
    textArea.classList.add("fc__text");
    return textArea;
  }

  /**
   * Get the position of the panel relative to the viewport.
   * @returns {Object} An object containing the top and left position.
   * @property {string} top - The vertical position of the comment in pixels (px).
   * @property {string} left - The horizontal position of the comment in viewport width units (vw).
   */
  _getPos() {
    const relativeLeft =
      this._panel.offsetLeft / ((window.innerWidth + window.pageXOffset) / 100);

    return {
      top: this._panel.style.top,
      left: relativeLeft + "vw",
    };
  }
}

export default Panel;
