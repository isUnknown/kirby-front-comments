<?php

namespace AdrienPayet\FrontComments;

class Comment
{
    private $page;
    private $data;
    private $options;

    public function __construct($commentData, $page)
    {
        $this->page = $page;
        $this->data = $commentData;
        $this->options = option('adrienpayet.front-comments');

        if ($this->options['repo.service']) {
            $this->options['repo.service'] = strtolower($this->options['repo.service']);
            $this->options['repo.name'] = strtolower($this->options['repo.name']);
            $this->options['repo.name'] = str_replace(' ', '-', $this->options['repo.name']);
        }
    }


    private function _getCreateIssueRequestUrl()
    {
        $id = urlencode($this->options['repo.owner'] . '/' . $this->options['repo.name']);

        $baseUrl = null;
        if ($this->options['repo.service'] === 'github') {
            $baseUrl = "https://api.github.com/repos/";
        } elseif ($this->options['repo.service'] === 'framagit') {
            $baseUrl = "https://framagit.org/api/v4/projects/";
        } elseif ($this->options['repo.service'] === 'gitlab') {
            $baseUrl = "https://gitlab.com/api/v4/projects/";
        }

        $url = null;
        if ($this->options['repo.service'] === 'github') {
            $url = $baseUrl . $id . "/issues?access_token=" . $this->options['repo.token'];
        } else {
            $url = $baseUrl . $id . "/issues?private_token=" . $this->options['repo.token'];
        }

        return $url;
    }

    private function _formatData()
    {
        $description = $this->_createDescription();

        $data = array(
          'title' => $this->data['message'],
          'labels' => option('adrienpayet.front-comments.labels')
        );

        if ($this->options['repo.service'] === 'github') {
            $data['body'] = $description;
        } else {
            $data['description'] = $description;
        }

        return $data;
    }

    private function _createDescription()
    {
        $description = 'Comment created by **' . $this->data['author'] . '** ';
        $description .= 'the **' . $this->data['date'] . '** at ' . $this->data['time'] . ' ';
        $description .= ' in a window of **' . $this->data['windowWidth'] . 'px width**. ';
        $description .= 'User agent: ' . $this->data['userAgent'] . ' . ';
        $description .= '[**SEE THE COMMENT**](' . site()->url() . '/' . $this->page->uri() . '/#' . $this->data['id'] .')';
        return $description;
    }

    public function createIssue()
    {
        $requestUrl = $this->_getCreateIssueRequestUrl();
        $data = $this->_formatData();

        $curl = curl_init();

        curl_setopt_array($curl, [
            CURLOPT_URL => $requestUrl,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => json_encode($data),
            CURLOPT_HTTPHEADER => [
                'Content-Type: application/json',
            ],
        ]);

        $response = curl_exec($curl);
        $httpCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);

        if ($httpCode != 201) {
            throw new Exception("Failed to create issue: {$httpCode}", 1);
        }

        curl_close($curl);

        $responseData = json_decode($response);

        if (isset($responseData->id)) {
            $this->data['issue-id'] = $responseData->iid;
            $this->data['issue-url'] = $this->options['repo.service'] === 'github' ? $responseData->html_url : $responseData->web_url;
        } else {
            throw new Exception("Invalid response format", 1);
        }
    }


    public function closeIssue()
    {
        $requestUrl = $this->_getCloseIssueRequestUrl();

        $data = null;
        if ($this->options['repo.service'] === 'github') {
            $data = json_encode([
                'state' => 'closed'
            ]);
        } else {
            $data = json_encode([
                'state_event' => 'close'
            ]);
        }

        $curl = curl_init();

        curl_setopt_array($curl, [
            CURLOPT_URL => $requestUrl,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => $this->options['repo.service'] === 'github' ? 'PATCH' : 'PUT',
            CURLOPT_POSTFIELDS => $data,
            CURLOPT_HTTPHEADER => [
                'Content-Type: application/json',
            ],
        ]);

        $response = curl_exec($curl);
        $httpCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);

        if ($httpCode != 200) {
            throw new Exception("Failed to close issue: {$httpCode}", 1);
        }

        curl_close($curl);
    }

    private function _getCloseIssueRequestUrl()
    {
        $id = urlencode($this->options['repo.owner'] . '/' . $this->options['repo.name']);

        $baseUrl = null;
        if ($this->options['repo.service'] === 'github') {
            $baseUrl = "https://api.github.com/repos/";
        } elseif ($this->options['repo.service'] === 'framagit') {
            $baseUrl = "https://framagit.org/api/v4/projects/";
        } elseif ($this->options['repo.service'] === 'gitlab') {
            $baseUrl = "https://gitlab.com/api/v4/projects/";
        }

        $issueId = $this->data['issue-id'];

        $url = null;
        if ($this->options['repo.service'] === 'github') {
            $url = $baseUrl . $id . "/issues/" . $issueId . "?access_token=" . $this->options['repo.token'];
        } else {
            $url = $baseUrl . $id . "/issues/" . $issueId . "?private_token=" . $this->options['repo.token'];
        }

        return $url;
    }


    public function id()
    {
        return $this->data['id'];
    }

    public function data()
    {
        return $this->data;
    }

    public function hasIssue()
    {
        return isset($this->data['issue-id']) && strlen($this->data['issue-id']) > 0;
    }

}
