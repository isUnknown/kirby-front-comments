<?php

F::loadClasses([
  'AdrienPayet\\FrontComments\\Comment' => 'classes/Comment.php'
], __DIR__);

require_once __DIR__ . '/lib/functions.php';

Kirby::plugin('adrienpayet/front-comments', [
  'translations' => [
    'en' => require_once(__DIR__ . '/translations/en.php'),
    'fr' => require_once(__DIR__ . '/translations/fr.php')
    ],
  'options' => [
    'cache' => true,
    'repo.service' => null,
    'repo.token' => null,
    'repo.owner' => null,
    'repo.name' => null,
    'repo.labels' => ['front-comments']
  ],
  'blueprints' => [
    'fields/front-comments/team' => __DIR__ . '/blueprints/fields/team.yml'
  ],
  'icons' => [],
  'areas' => [
    'comments' => function ($kirby) {
        return [
          'label' => t('adrienpayet.front-comments.comments'),
          'icon' => 'chat',
          'menu' => true,
          'link' => 'comments',
          'dropdowns' => [
            require __DIR__ . '/dropdowns/comment.php'
          ],
          'dialogs' => [
            require __DIR__ . '/dialogs/delete-comment.php',
            require __DIR__ . '/dialogs/create-issue.php'
          ],
          'views' => [
            [
              'pattern' => 'comments',
              'action'  => function () {
                  return [
                    'component' => 'k-comments-view',
                    'title' => t('adrienpayet.front-comments.comments'),
                    'props' => [
                      'pages' => function () {
                          $pages = array_values(kirby()->cache('adrienpayet.front-comments')->getOrSet('commented-pages', function () { return [] ;}));
                          return $pages;
                      },
                      'csrf' => csrf()
                    ],
                  ];
              }
            ]
          ],
        ];
    }
  ],
  'snippets' => [
    'front-comments' => __DIR__ . '/snippets/front-comments.php',
    'front-comments-script' => __DIR__ . '/snippets/front-comments-script.php',
    'front-comments-style' => __DIR__ . '/snippets/front-comments-style.php',
    'front-comments-field' => __DIR__ . '/snippets/front-comments-field.php',
  ],
  'routes' => [
    [
      'pattern' => '/store-comment.json',
      'method' => 'POST',
      'action' => function () {
          include_once __DIR__ . '/routes/store-comment.php';
          return storeComment();
      }
    ],
    [
      'pattern' => '/comments/delete/(:any)/(:all).json',
      'method' => 'PATCH',
      'action' => function ($commentId, $pageUri) {
          return json_encode(deleteComment($pageUri, $commentId));
      }
    ]
    ],
  'hooks' => [
    'page.update:after' => function ($newPage) {
        cacheFrontComments($newPage);
    }
  ]
]);
