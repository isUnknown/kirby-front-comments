<?php

use AdrienPayet\FrontComments\Comment;

return [
    'pattern' => 'comments/create-issue/(:any)/(:all)',
    'load' => function (string $commentId, string $pageUri) {
        return [
            'component' => 'k-text-dialog',
            'props' => [
                'text' => t('adrienpayet.front-comments.confirm-create-issue')
            ]
        ];
    },
    'submit' => function (string $commentId, string $pageUri) {

        $page = Find::page($pageUri);
        $comments = $page->comments()->toData('yaml');

        $commentData = getCommentData($comments, $commentId);

        $comment = new Comment($commentData, $page);

        try {
            $comment->createIssue();
            $comments = array_map(
                function ($item) use ($comment) {
                    if ($item['id'] == $comment->id()) {
                        return $comment->data();
                    }
                    return $item;
                },
                $comments
            );
            $page->update(
                [
                  'comments' => $comments
                ]
            );
            return true;
        } catch (\Throwable $th) {
            throw new Exception($th->getMessage(), 1);
        }
    }
];
